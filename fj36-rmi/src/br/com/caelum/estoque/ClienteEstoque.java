package br.com.caelum.estoque;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

public class ClienteEstoque {


    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        ServicoDeEstoque servicoDeEstoque = (ServicoDeEstoque) Naming.lookup("rmi://localhost:1099/estoque");

        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String codigoProduto = scanner.nextLine();
            Optional<ItemEstoque> item = servicoDeEstoque.consultaProduto(codigoProduto);

            System.out.println(item.map(ItemEstoque::getQuantidade).orElse(0));
            /* java 7

            if (Objects.isNull(item)) {
                System.out.println("Produto " + codigoProduto + " não existe");
            } else {
                System.out.println("Quantidade em estoque: " + item.getQuantidade());
            }*/
        }
    }
}
