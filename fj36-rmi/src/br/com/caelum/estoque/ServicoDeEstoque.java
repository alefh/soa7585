package br.com.caelum.estoque;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Optional;

public interface ServicoDeEstoque extends Remote {

    Optional<ItemEstoque> consultaProduto(String codigoProduto)
            throws RemoteException;
}
