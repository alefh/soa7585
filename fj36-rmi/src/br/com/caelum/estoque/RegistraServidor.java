package br.com.caelum.estoque;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RegistraServidor {

    public static void main(String[] args) throws RemoteException, MalformedURLException {

        LocateRegistry.createRegistry(1099);
        Naming.rebind("/estoque", new ServidorDeEstoque());
        System.out.println("SERVICO DE ESTOQUE RODANDO NA PORTA 1099");
    }
}




