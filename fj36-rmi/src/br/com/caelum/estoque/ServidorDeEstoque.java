package br.com.caelum.estoque;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ServidorDeEstoque extends UnicastRemoteObject
        implements ServicoDeEstoque{

    private Map<String, ItemEstoque> bancoDeDadosDoEstoque = new HashMap<>();

    public ServidorDeEstoque() throws RemoteException{
        bancoDeDadosDoEstoque.put("TDD", new ItemEstoque("TDD", 10));
        bancoDeDadosDoEstoque.put("SOA", new ItemEstoque("SOA", 5));
        bancoDeDadosDoEstoque.put("WEB", new ItemEstoque("WEB", 20));
        bancoDeDadosDoEstoque.put("DDD", new ItemEstoque("DDD", 12));
        bancoDeDadosDoEstoque.put("JAVA", new ItemEstoque("JAVA", 10));
    }

    @Override
    public Optional<ItemEstoque> consultaProduto(String codigoProduto) throws RemoteException {
        return Optional.of(bancoDeDadosDoEstoque.get(codigoProduto));
    }

    public void deletaEstoque(String codigoProduto) {
        bancoDeDadosDoEstoque.remove(codigoProduto);
    }
}
