
package br.com.caelum.estoque.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.caelum.estoque.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Token_QNAME = new QName("http://caelum.com.br/estoquews/v1", "token");
    private final static QName _QuantidadeEmEstoque_QNAME = new QName("http://caelum.com.br/estoquews/v1", "quantidadeEmEstoque");
    private final static QName _QuantidadeEmEstoqueResponse_QNAME = new QName("http://caelum.com.br/estoquews/v1", "quantidadeEmEstoqueResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.caelum.estoque.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QuantidadeEmEstoqueResponse }
     * 
     */
    public QuantidadeEmEstoqueResponse createQuantidadeEmEstoqueResponse() {
        return new QuantidadeEmEstoqueResponse();
    }

    /**
     * Create an instance of {@link QuantidadeEmEstoque }
     * 
     */
    public QuantidadeEmEstoque createQuantidadeEmEstoque() {
        return new QuantidadeEmEstoque();
    }

    /**
     * Create an instance of {@link ItemEstoque }
     * 
     */
    public ItemEstoque createItemEstoque() {
        return new ItemEstoque();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caelum.com.br/estoquews/v1", name = "token")
    public JAXBElement<String> createToken(String value) {
        return new JAXBElement<String>(_Token_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuantidadeEmEstoque }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caelum.com.br/estoquews/v1", name = "quantidadeEmEstoque")
    public JAXBElement<QuantidadeEmEstoque> createQuantidadeEmEstoque(QuantidadeEmEstoque value) {
        return new JAXBElement<QuantidadeEmEstoque>(_QuantidadeEmEstoque_QNAME, QuantidadeEmEstoque.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuantidadeEmEstoqueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caelum.com.br/estoquews/v1", name = "quantidadeEmEstoqueResponse")
    public JAXBElement<QuantidadeEmEstoqueResponse> createQuantidadeEmEstoqueResponse(QuantidadeEmEstoqueResponse value) {
        return new JAXBElement<QuantidadeEmEstoqueResponse>(_QuantidadeEmEstoqueResponse_QNAME, QuantidadeEmEstoqueResponse.class, null, value);
    }

}
