package br.com.caelum.estoque.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServicoEstoque extends
        Remote {

    ItemEstoque consultaEstoque(String codigoProduto) throws RemoteException;
}
